/**
 * 创建路由对象（还要导出去，在入口文件main.js中去注入）、设置路由规则（根据实际情况进行设置）
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Singer from '@/views/Singer'
import Songs from '@/views/Songs'
import SingerDetail from '@/views/SingerDetail'
import SongDetail from '@/views/SongDetail'
import NotFound from '@/views/NotFound'
// .use是在使用Vue插件，至于原理，后续项目中会涉及
Vue.use(VueRouter)

// 创建路由对象（Vue2中很多东西都是new出来的，因为Vue2是面向对象编程）
const router = new VueRouter({
  // mode: 'history', // 路由模式，默认是hash
  // 这里面就是路由规则
  routes: [
    // path是访问的路径，component是对应的页面
    { path: '/singer', component: Singer },
    { path: '/songs', component: Songs },
    // { path: '/', component: Songs }
    // 浏览器访问/的时候，会再次跳转到/songs
    { path: '/', redirect: '/songs' },
    { path: '/singerdetail', component: SingerDetail },
    // parmas传参时候，路由规则设置有所不一样！！！
    // 动态路径参数 https://v3.router.vuejs.org/zh/guide/essentials/dynamic-matching.html
    {
      path: '/songdetail/:id/:name',
      component: SongDetail
    },
    // 404配置一定要放在最后，前面的匹配到了，就不会来到404
    { path: '*', component: NotFound }
  ]
})

// 导出去，因为在入口main.js中要注入
export default router
