/**
 * 创建路由对象（还要导出去，在入口文件main.js中去注入）、设置路由规则（根据实际情况进行设置）
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Layout from '@/views/Layout'
import User from '@/views/User'
import Swiper from '@/views/Swiper'
// import Axios from '@/views/Axios'
import VuexBase from '@/views/VuexBase'
import NotFound from '@/views/NotFound'
// .use是在使用Vue插件，至于原理，后续项目中会涉及
Vue.use(VueRouter)

// 创建路由对象（Vue2中很多东西都是new出来的，因为Vue2是面向对象编程）
const router = new VueRouter({
  // mode: 'history', // 路由模式，默认是hash
  // 这里面就是路由规则
  routes: [
    { path: '/login', component: Login },
    {
      path: '/layout',
      component: Layout,
      // redirect: '/layout/swiper',
      children: [
        // { path: '', component: User },
        // 子路由前面不要加 /，它会把它当成跟根路径处理 https://v3.router.vuejs.org/zh/guide/essentials/nested-routes.html
        { path: 'user', component: User },
        { path: 'swiper', component: Swiper }
      ]
    },
    { path: '/', redirect: '/login' },
    // { path: '/axios', component: Axios },
    { path: '/vuexbase', component: VuexBase },
    // 404配置一定要放在最后，前面的匹配到了，就不会来到404
    { path: '*', component: NotFound }
  ]
})

// 导出去，因为在入口main.js中要注入
export default router
