import Vue from 'vue'

// 创建一个公共的Vue实例
const bus = new Vue()

// 导出公共的bus
export default bus
