// 引入vue，有点类似于<script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
// 只不过这里用的是模块的方式
import Vue from 'vue'
// @ 代表 src，这个脚手架已经配置好了，如果我们的文件名叫做 index 可以省略不写，默认找到的就是index
import router from '@/router'
import store from '@/store'

// 引入的是根组件（一个项目中只有一个,他就是直接放在id=app div里面的）
import App from './App.vue'

// 创建项目的根实例（一个项目中有且仅有一个）
// .$mount('#app') 等价于 el: '#app' 是代表把根组件（App.vue）渲染到id=app的div里面去
new Vue({
  // el: '#app',
  // 渲染：渲染根组件，到id=app的div中去
  render: h => h(App),
  router, // 注入路由到根实例中
  store // 注入仓库到根实例
}).$mount('#app')

// 总结：导入Vue，创建根实例，然后把根组件(App.vue)渲染到id=app的div中去【必须得做】
