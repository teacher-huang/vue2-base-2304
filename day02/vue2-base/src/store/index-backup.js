import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 创建仓库
const store = new Vuex.Store({
  // 定义要操作的数据
  state: {
    count: 0
  },
  //
  getters: {
    // state就是上面的state
    // getters里面的方法，相当于组件中的computed
    getDoubleCount (state) {
      return state.count * 2
    }
  },
  // 同步
  mutations: {
    /**
     *
     * @param {*} state 参数1：上面的state
     * @param {*} payload 参数2：载荷 === 参数
     */
    add (state, payload) {
      state.count += payload
    }
  },
  // 异步
  actions: {
    asyncAdd (context, payload) {
      // 支持异步的代码
      setTimeout(() => {
        context.commit('add', payload)
      }, 2000)
    }
  }
})

// 导出去，要在入口文件中，注入
export default store
