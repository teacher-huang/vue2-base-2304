import Vue from 'vue'
import Vuex from 'vuex'

// 导入模块
import counter from './modules/counter'
import user from './modules/user'

Vue.use(Vuex)

// 创建仓库
const store = new Vuex.Store({
  // 注册模块
  modules: {
    counter,
    user
  }
})

// 导出去，要在入口文件中，注入
export default store
