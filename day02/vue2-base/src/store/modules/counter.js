export default {
  namespaced: true, // 开启命名空间（仓库中的这个counter小房间上锁了）
  // 定义counter模块中，要操作的数据
  state: {
    count: 0
  },
  // 需要经过计算的时候，并且它有缓存
  getters: {
    getCountSquare (state) {
      return state.count * state.count
    }
  },
  mutations: {
    /**
     *
     * @param {*} state 这个是state
     * @param {*} payload 参数（载荷）
     */
    add (state, payload) {
      state.count += payload
    }
  },
  actions: {
    // context是上下文，里面有一个方法是 commit【commit就可以调用mutations中的方法】
    asyncAdd (context, payload) {
      // 先做异步操作（比如发请求，然后调用mutations的方法，把数据保存起来）
      setTimeout(() => {
        /**
         * 参数1：mutations方法中的名字
         * 参数2：参数
         */
        context.commit('add', payload)
      }, 1000)
    }
  }
}
