export default {
  namespaced: true, // 开启命名空间
  state: {
    userInfo: {
      token: '' // 令牌（登录成功之后，一般后台服务器会返回，我们到时候，拿着服务器返回的token，设置给仓库中user模块的userInfo.token）
    }
  },
  getters: {
    // 类似于组件中的计算属性，他有缓存
    getToken (state) {
      return state.userInfo.token
    }
  },
  mutations: {
    setToken (state, payload) {
      state.userInfo.token = payload
    }
  }
}
